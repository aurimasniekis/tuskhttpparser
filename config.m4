PHP_ARG_ENABLE(tuskhttpparser,
    [Whether to enable the "TuskHttpParser" extension],
    [  --enable-tuskhttpparser      Enable "TuskHttpParser" extension support])

if test $PHP_TUSKHTTPPARSER != "no"; then
    PHP_NEW_EXTENSION(tuskhttpparser, tuskhttpparser.c http11_parser.c, $ext_shared)
fi