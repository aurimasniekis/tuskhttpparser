#ifndef PHP_TUSK_HTTP_PARSER_H
#define PHP_TUSK_HTTP_PARSER_H

#define PHP_TUSK_HTTP_PARSER_EXTNAME  "TuskHttpParser"
#define PHP_TUSK_HTTP_PARSER_EXTVER   "1.0"

#include "php.h"

extern zend_module_entry tusk_http_parser_module_entry;

#endif /* PHP_TUSK_HTTP_PARSER_H */